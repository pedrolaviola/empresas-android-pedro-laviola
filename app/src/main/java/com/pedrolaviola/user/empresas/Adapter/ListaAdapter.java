package com.pedrolaviola.user.empresas.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pedrolaviola.user.empresas.R;
import com.pedrolaviola.user.empresas.models.Enterprises;
import com.pedrolaviola.user.empresas.models.ListaEnterprise;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 07/10/2017.
 */

public class ListaAdapter extends RecyclerView.Adapter<ListaAdapter.ViewHolder>{

    private List<Enterprises> list;
    private Context context;
    private OnItemClickInterface onItemClickInterface;
    //Constructor
    public ListaAdapter(Context context,OnItemClickInterface onItemClickInterface) {
        this.context = context;
        this.onItemClickInterface = onItemClickInterface;
        list=new ArrayList<Enterprises>();
    }
    public void setList(List<Enterprises> list){
        this.list = list;
        notifyDataSetChanged();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_lista, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Enterprises enterprises =list.get(position);
        holder.item_name.setText(enterprises.getEnterprise_name());
        holder.item_country.setText(enterprises.getCountry());
        holder.item_type.setText(enterprises.getEnterprise_type().getEnterprise_type_name());

        //Item click
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickInterface.onItemClick(list.get(holder.getAdapterPosition()));
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private TextView item_name;
        private TextView item_type;
        private TextView item_country;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.item_name = (TextView) view.findViewById(R.id.item_name);
            this.item_type = (TextView) view.findViewById(R.id.item_type);
            this.item_country = (TextView) view.findViewById(R.id.item_country);
        }
    }
    public interface OnItemClickInterface {
        void onItemClick(Enterprises item);
    }
}
