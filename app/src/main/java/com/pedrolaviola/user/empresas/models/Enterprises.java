package com.pedrolaviola.user.empresas.models;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by User on 07/10/2017.
 */

public class Enterprises implements Serializable{
    private String id;
    private String enterprise_name;
    private String description;
    private String country;
    private Enterprise_type enterprise_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Enterprise_type getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(Enterprise_type enterprise_type) {
        this.enterprise_type = enterprise_type;
    }
}
