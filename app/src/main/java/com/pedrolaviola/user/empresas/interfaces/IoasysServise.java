package com.pedrolaviola.user.empresas.interfaces;

import com.pedrolaviola.user.empresas.models.ListaEnterprise;
import com.pedrolaviola.user.empresas.models.UserRequest;
import com.pedrolaviola.user.empresas.models.UserResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by User on 02/10/2017.
 */

public interface IoasysServise {

    static final String BASE_URL =" http://54.94.179.135:8090/api/v1/";
    static final String SHARED_KEY="Arquvo_token";

    @POST("users/auth/sign_in")
    Call<UserResponse>getUserAccess(@Body UserRequest userRequest);
    @GET("enterprises")
    Call<ListaEnterprise>getEnterprises(@Header("access-token")String access_token, @Header("client")String client,
                                        @Header("uid")String uid);

}

