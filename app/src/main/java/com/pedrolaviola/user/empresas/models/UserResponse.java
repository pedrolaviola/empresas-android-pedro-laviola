package com.pedrolaviola.user.empresas.models;

/**
 * Created by User on 02/10/2017.
 */

public class UserResponse {

   private String investor_name;
   private  String email;

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
