package com.pedrolaviola.user.empresas.models;

import java.io.Serializable;

/**
 * Created by User on 04/10/2017.
 */

public class AccessToken implements Serializable {

    private String access_token;
    private String client;
    private String uid;

    public AccessToken() {

    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
