package com.pedrolaviola.user.empresas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.pedrolaviola.user.empresas.Adapter.ListaAdapter;
import com.pedrolaviola.user.empresas.interfaces.IoasysServise;
import com.pedrolaviola.user.empresas.models.AccessToken;
import com.pedrolaviola.user.empresas.models.Enterprises;
import com.pedrolaviola.user.empresas.models.ListaEnterprise;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ListaFragment extends Fragment implements ListaAdapter.OnItemClickInterface {

    private ListaEnterprise listaEnterprise;
    private ListaAdapter adapter;
    private EditText search_Edt;
    private RelativeLayout fragment;

    public ListaFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_lista, container, false);

        //Cast da lista e da tela
        search_Edt = (EditText) view.findViewById(R.id.id_search_Edt);
        fragment = (RelativeLayout) view.findViewById(R.id.fragment);
        final RecyclerView lista_empresas = (RecyclerView) view.findViewById(R.id.recycler_view);
        //Recycler View
        lista_empresas.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ListaAdapter(getContext(), this);
        lista_empresas.setAdapter(adapter);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(IoasysServise.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        AccessToken accessToken = new AccessToken();
        //Buscar o Access-Token do shared preferences
        SharedPreferences shared = this.getActivity().getSharedPreferences(IoasysServise.SHARED_KEY,0);
        accessToken.setAccess_token(shared.getString("access_token","sem Token"));
        accessToken.setClient(shared.getString("client","Sem Client"));
        accessToken.setUid(shared.getString("uid","Sem Uid"));
        //call
        final IoasysServise serviseEnterprises = retrofit.create(IoasysServise.class);
        Call<ListaEnterprise>enterprisesCall = serviseEnterprises.getEnterprises(accessToken.getAccess_token(),
                accessToken.getClient(),accessToken.getUid());

        enterprisesCall.enqueue(new Callback<ListaEnterprise>() {
            @Override
            public void onResponse(Call<ListaEnterprise> call, Response<ListaEnterprise> response) {

                if(response.isSuccessful()){
                    listaEnterprise=response.body();

                }
                else{
                    Toast.makeText(getContext(),"Token negado",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ListaEnterprise> call, Throwable t) {
                Toast.makeText(getContext(),"Sem resposta",Toast.LENGTH_SHORT).show();
            }
        });

        search_Edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String nome_enterprise = search_Edt.getText().toString();
                //Get Listas que contem o EditText
                List<Enterprises> lista = getEnterprises(nome_enterprise);
                adapter.setList(lista);
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        search_Edt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (search_Edt.getRight() - search_Edt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        fragment.setVisibility(View.GONE);
                        return true;
                    }
                }
                return false;
            }
        });
        return view;
    }


    private List<Enterprises> getEnterprises(String nome_enterprise) {
        List<Enterprises> lista =new ArrayList<Enterprises>();
        for(int i=0;i<listaEnterprise.enterprises.size();i++){
            Enterprises empresa =(Enterprises) listaEnterprise.enterprises.get(i);

            if(empresa.getEnterprise_name().startsWith(nome_enterprise)){
                lista.add(empresa);
            }
        }
        return lista;
    }

    public void onItemClick(Enterprises enterprises){
        Intent intent =new Intent(getContext(),DetailActivity.class);
        intent.putExtra(DetailActivity.EXTRA_KEY,enterprises);
        startActivity(intent);
    }


}
