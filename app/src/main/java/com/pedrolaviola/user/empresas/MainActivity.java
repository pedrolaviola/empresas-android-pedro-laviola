package com.pedrolaviola.user.empresas;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pedrolaviola.user.empresas.interfaces.IoasysServise;
import com.pedrolaviola.user.empresas.models.UserRequest;
import com.pedrolaviola.user.empresas.models.UserResponse;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity {

    private EditText edt_email;
    private EditText edt_password;
    private Button btn_signin;

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText)findViewById(R.id.edt_password);
        btn_signin = (Button) findViewById(R.id.btn_signin);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(IoasysServise.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        final IoasysServise servise = retrofit.create(IoasysServise.class);

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserRequest userRequest =new UserRequest();
                userRequest.setEmail(edt_email.getText().toString());
                userRequest.setPassword(edt_password.getText().toString());
                Call<UserResponse> tokenResponseCall= servise.getUserAccess(userRequest);

                tokenResponseCall.enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                            if(response.isSuccessful()){
                                SharedPreferences sharedPreferences = getSharedPreferences(IoasysServise.SHARED_KEY,0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                UserResponse userResponse = response.body();
                                Headers headers = response.headers();
                                String accessToken =headers.get("access-token");
                                String client = headers.get("client");
                                String uid = headers.get("uid");
                                editor.putString("access_token",accessToken);
                                editor.putString("client",client);
                                editor.putString("uid", uid);
                                editor.commit();
                                Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                                intent.putExtra("AccessToken", accessToken);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                            else{
                                Toast.makeText(getApplicationContext(),"Usuário invalido",Toast.LENGTH_SHORT).show();
                            }

                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


    }

}
