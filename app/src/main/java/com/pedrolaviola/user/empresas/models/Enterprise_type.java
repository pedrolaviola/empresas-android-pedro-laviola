package com.pedrolaviola.user.empresas.models;

import java.io.Serializable;

/**
 * Created by User on 07/10/2017.
 */

public class Enterprise_type implements Serializable {
    private String enterprise_type_name;
    private int id;

    public void setEnterprise_type_name(String enterprise_type_name) {
        this.enterprise_type_name = enterprise_type_name;
    }

    public String getEnterprise_type_name() {
        return enterprise_type_name;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
