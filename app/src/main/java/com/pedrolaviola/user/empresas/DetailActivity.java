package com.pedrolaviola.user.empresas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.pedrolaviola.user.empresas.models.Enterprises;

public class DetailActivity extends AppCompatActivity {

    static final String EXTRA_KEY = "empresa";
    private Enterprises enterprises;
    private TextView text_detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = getIntent();
        enterprises = (Enterprises) intent.getSerializableExtra(EXTRA_KEY);
        getSupportActionBar().setTitle(enterprises.getEnterprise_name());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        text_detail = (TextView)findViewById(R.id.id_text_detail);

        text_detail.setText(enterprises.getDescription());
        // Retorna para tela principal

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
