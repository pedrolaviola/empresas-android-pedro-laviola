package com.pedrolaviola.user.empresas.models;

/**
 * Created by User on 02/10/2017.
 */

public class UserRequest {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
